Socialite
====

My basic activities when I use FB are all in this little application:

- Post silly statuses
- Give comments
- My FB friends can do the same

No spam! No user activity tracking! No ads!

A docker image will be made available soon! Stay tuned!