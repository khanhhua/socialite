FROM node:slim

ENV MEDIA_SALT='' \
    DROPBOX_ACCESS_TOKEN=''  \
    DATABASE_URL='' \
    FB_APP_ID='' \
    FB_APP_SECRET='' \
    CORS='' \
    OAUTH_REDIRECT_BASE_URI='' \
    FRIEND_LIST='' \
    JWT_SECRET=''

EXPOSE 8080

WORKDIR /opt/app-root
COPY ./api .

WORKDIR /opt/app-root/api
RUN npm install
CMD ["npm", "start"]
