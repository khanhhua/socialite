// PnC :: Post and Comment
import 'isomorphic-fetch';
import { Dropbox } from 'dropbox';

import debug from 'debug';
import route from 'koa-route';
import { Post, Comment } from './db';

const dbg = debug('socialite:pnc');
const DEFAULT_FETCH_LIMIT = 10;

let dropbox;

async function getPosts(limit=DEFAULT_FETCH_LIMIT, anchor=null) {
  dbg(`Getting post for params limit=${limit} anchor=${anchor}`);
  let result;
  const sortOpts = {
    _id: -1
  };

  if (anchor) {
    result = await Post.find({ _id: { $lt: anchor }}).limit(limit).sort(sortOpts);
  } else {
    result = await Post.find({}).limit(limit).sort(sortOpts);
  }

  let promises = result.map(post => Promise.all(post.media.map(mediaItem => {
    return new Promise(async resolve => {
      const temporaryLink = await dropbox.filesGetTemporaryLink({path: `/${mediaItem}`});

      resolve(temporaryLink.link);
    })
  })).then(links => post.media = links));

  await Promise.all(promises);
  dbg(`Posts retrieved: ${result.length}`);

  return {
    ok: true,
    data: result
  };
}

async function getPost(id) {
  const result = await Post.findOne({ _id: id});
  await Promise.all(result.media.map(mediaItem => new Promise(async resolve => {
    const temporaryLink = await dropbox.filesGetTemporaryLink({ path: `/${mediaItem}` });
    resolve(temporaryLink.link);
  }).then(links => result.media = links)));
  dbg(`Post #${id} retrieved`);

  return {
    ok: true,
    data: result
  };
}

async function createPost(userId, data) {
  const post = new Post({ user_id: userId, ...data });

  await post.save();
  const result = await Post.findOne({ _id: post._id });
  await Promise.all(result.media.map(mediaItem => new Promise(async resolve => {
    const temporaryLink = await dropbox.filesGetTemporaryLink({ path: `/${mediaItem}` });
    resolve(temporaryLink.link);
  }).then(links => result.media = links)));

  return {
    ok: true,
    data: result
  };
}

async function createComment(userId, postId, data) {
  const comment = new Comment({ ...data, user_id: userId, post_id: postId });

  await comment.save();
  const result = await Comment.findOne({ _id: comment._id });

  return {
    ok: true,
    data: result
  };
}

async function getComments(postId, limit=DEFAULT_FETCH_LIMIT, anchor=null) {
  dbg(`limit = ${limit}\nanchor = ${anchor}`);

  const sortOpts = { _id: -1 };
  limit = limit || DEFAULT_FETCH_LIMIT;

  let result;
  if (anchor) {
    result = await Comment.find({ post_id: postId, _id: { $lt: anchor }}).limit(limit).sort(sortOpts);
  } else {
    result = await Comment.find({ post_id: postId }).limit(limit).sort(sortOpts);
  }

  return {
    ok: true,
    data: result.reverse()
  }
}

export default function (app, baseUrl) {
  dropbox = new Dropbox({ accessToken: process.env.DROPBOX_ACCESS_TOKEN });

  app.use(route.get(`${baseUrl}/posts`, async function (ctx) {
    const { anchor = null } = ctx.request.query;

    ctx.body = await getPosts(DEFAULT_FETCH_LIMIT, anchor);
  }));
  app.use(route.post(`${baseUrl}/posts`, async (ctx) => {
    const userId = ctx.state.oauth.id;
    ctx.body = await createPost(userId, ctx.request.body)
  }));
  app.use(route.get(`${baseUrl}/posts/:id`, async function (ctx, id) {
    ctx.body = await getPost(id);
  }));

  app.use(route.post(`${baseUrl}/posts/:id/comments`, async function (ctx, id) {
    dbg(ctx.state.oauth);
    const userId = ctx.state.oauth.id;

    ctx.body = await createComment(userId, id, ctx.request.body);
  }));
  app.use(route.get(`${baseUrl}/posts/:id/comments`, async function (ctx, id) {
    const { anchor = null } = ctx.request.query;

    ctx.body = await getComments(id, 0, anchor);
  }));
}