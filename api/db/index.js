import { default as mongoose } from 'mongoose';
export { default as Post } from './Post';
export { default as Comment } from './Comment';

export function connect(databaseURL) {
  console.log(`Using database URL: ${databaseURL}`);
  let connection;

  return async function(ctx, next) {
    if (connection) {
      await next();
      return;
    }

    connection = await mongoose.connect(databaseURL, { useNewUrlParser: true });
    await next();
    return;
  };
}