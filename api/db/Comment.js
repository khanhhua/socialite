import { default as mongoose, Schema } from 'mongoose';
import {
  ERROR_CONTENT_MSG,
  ERROR_CONTENT_VIOL,
  ERROR_EMPTY_MSG,
  ERROR_EMPTY_VIOL,
  ERROR_LONG_TEXT_MSG,
  ERROR_LONG_TEXT_VIOL, ERROR_MANY_PHOTOS_MSG, ERROR_MANY_PHOTOS_VIOL,
  makeError,
  RULE_CHARS_LIMIT, RULE_FILE_UPLOAD_LIMIT
} from './validations';
import { checkText } from './validations';

const CommentSchema = new Schema({
  user_id: String,
  post_id: 'ObjectId',
  content: String
});
CommentSchema.virtual('id').get(function() {
  return this._id.toHexString();
});
CommentSchema.virtual('user').get(function () {
  if (this.user_id in FRIENDS) {
    let user = FRIENDS[this.user_id];
    user.id = this.user_id;

    return user;
  }

  return {
    id: this.user_id
  };
});
CommentSchema.set('toJSON', { getters: true, virtuals: true });

CommentSchema.pre('save', function (next) {
  let errors = [];
  const content = this.content;

  if (!content) {
    errors.push(makeError(ERROR_EMPTY_VIOL, ERROR_EMPTY_MSG));
  } else {
    if (content.length > RULE_CHARS_LIMIT) {
      errors.push(makeError(ERROR_LONG_TEXT_VIOL, ERROR_LONG_TEXT_MSG));
    }

    if (!checkText(content)) {
      errors.push(makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG));
    }
    // Content issues
    if (!(/^[\w\s!@#$%^&*():;"'<=>+.,?\/]*$/gm.test(content))) {
      errors.push(makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG));
    }
  }

  if (errors.length) {
    next(errors);
  } else {
    next();
  }
});

export default mongoose.model('comment', CommentSchema);