import { default as mongoose, Schema } from 'mongoose';
import { checkText } from './validations';
import {
  makeError,
  ERROR_CONTENT_MSG,
  ERROR_CONTENT_VIOL,
  ERROR_EMPTY_VIOL,
  ERROR_LONG_TEXT_MSG,
  RULE_CHARS_LIMIT,
  ERROR_LONG_TEXT_VIOL,
  ERROR_MANY_PHOTOS_VIOL,
  ERROR_MANY_PHOTOS_MSG, RULE_FILE_UPLOAD_LIMIT, ERROR_EMPTY_MSG
} from './validations';

const PostSchema = new Schema({
  user_id: {
    type: String,
    required: true
  },
  content: String,
  media: [String]
});
PostSchema.virtual('id').get(function() {
  return this._id.toHexString();
});
PostSchema.virtual('user').get(function () {
  let user = global.friendCache.get(this.user_id);
  if (user) {
    return user;
  }

  return {
    id: this.user_id
  };
});
PostSchema.set('toJSON', { getters: true, virtuals: true });

PostSchema.pre('save', function (next) {
  let errors = [];
  const content = this.content;
  const media = this.media;

  if (!content && (!media || !media.length)) {
    errors.push(makeError(ERROR_EMPTY_VIOL, ERROR_EMPTY_MSG));
  } else {
    if (content) {
      if (content.length > RULE_CHARS_LIMIT) {
        errors.push(makeError(ERROR_LONG_TEXT_VIOL, ERROR_LONG_TEXT_MSG));
      }

      if (!checkText(content)) {
        errors.push(makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG));
      }
      // Content issues
      if (!(/^[\w\s!@#$%^&*():;"'<=>+.,?\/]*$/gm.test(content))) {
        errors.push(makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG));
      }
    }

    if (media.length > RULE_FILE_UPLOAD_LIMIT) {
      errors.push(makeError(ERROR_MANY_PHOTOS_VIOL, ERROR_MANY_PHOTOS_MSG));
    }
  }

  if (errors.length) {
    next(errors);
  } else {
    next();
  }
});

export default mongoose.model('post', PostSchema);