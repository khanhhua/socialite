Socialite
====

My basic activities when I use FB are all in this little application:

- Post silly statuses
- Give comments
- My FB friends can do the same

No spam! No user activity tracking! No ads!

# Related

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
