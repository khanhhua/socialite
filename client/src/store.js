import { observable, computed, action } from "mobx";
import FBSDK from 'fb-sdk';

const API_BASEURL = `${process.env.REACT_APP_API_HOST}/api`;
const OAUTH_REDIRECT_BASE_URI = process.env.REACT_APP_OAUTH_REDIRECT_BASE_URI;
/**
 *
 */
class User {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

/**
 *
 */
class Post {
  @observable content = null;
  @observable user = null;
  @observable comments = [];
  media = [];

  @observable reachedBOF = false;
  @observable isCommentsVisible = false;
  @observable isCommenting = false;

  anchor = null;

  constructor() {
    this.id = null;
  }

  @action addComment(data) {
    if (!SessionStore.instance().isLoggedIn) {
      throw new Error('Authorization required');
    }

    return fetch(`${API_BASEURL}/posts/${this.id}/comments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${SessionStore.instance().accessToken}`,
      },
      body: JSON.stringify(data)
    }).then(res => {
      if (res.ok) {
        return res.json()
      } else {
        return {};
      }
    }).then(body => {
      if (!body.ok) {
        return;
      }

      const { data: { id, content, user }} = body;
      const comment = new Comment({ id, content, user: new User(user.id, user.name) });
      this.anchor = id;

      this.comments.push(comment);
      return true;
    })
  }

  /**
   *
   * @returns {Promise<Response>}
   */
  @action loadComments() {
    if (!SessionStore.instance().isLoggedIn) {
      throw new Error('Authorization required');
    }

    const url = new URL(`${API_BASEURL}/posts/${this.id}/comments`);
    if (this.anchor) {
      url.searchParams.append('anchor', this.anchor);
    }

    return fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${SessionStore.instance().accessToken}`,
      }
    }).then(res => {
      if (res.ok) {
        return res.json()
      } else {
        return {};
      }
    }).then(body => {
      if (!body.ok) {
        return;
      }
      if (!body.data.length) {
        this.reachedBOF = true;
        return;
      }

      this.anchor = body.data[0].id;

      body.data.forEach((data, index) => {
        const { id, content, user } = data;
        const comment = new Comment({ id, content, user });

        this.comments.splice(index, 0, comment);
      });
    })
  }
}

/**
 *
 */
class Comment {
  id = null;
  user = null;
  @observable content = null;

  constructor({id, user, content}) {
    this.id = id;
    this.content = content;
    this.user = user;
  }
}

/**
 *
 */
class StoryLine {
  @observable visiblePosts = [];

  anchor = null;

  @action append(post) {
    this.visiblePosts.unshift(post);
  }

  @action refresh() {
    if (!SessionStore.instance().isLoggedIn) {
      throw new Error('Authorization required');
    }

    return fetch(`${API_BASEURL}/posts`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${SessionStore.instance().accessToken}`,
      }
    }).then(res => {
      if (res.ok) {
        return res.json()
      } else {
        return {};
      }
    }).then(body => {
      if (!body.ok) {
        return;
      }

      if (body.data.length) {
        this.anchor = body.data[body.data.length - 1].id;
      }

      body.data.forEach(data => {
        const post = new Post();
        const { id, content, media } = data;
        post.id = id;
        post.content = content;
        post.media = media;
        post.user = new User(data.user.id, data.user.name);

        this.visiblePosts.push(post);
      });
    })
  }

  @action loadmore() {
    if (!SessionStore.instance().isLoggedIn) {
      throw new Error('Authorization required');
    }

    const url = new URL(`${API_BASEURL}/posts`);
    if (this.anchor) {
      url.searchParams.append('anchor', this.anchor);
    }

    return fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${SessionStore.instance().accessToken}`,
      }
    }).then(res => {
      if (res.ok) {
        return res.json()
      } else {
        return {};
      }
    }).then(body => {
      if (!body.ok) {
        return;
      }

      if (body.data.length) {
        this.anchor = body.data[body.data.length - 1].id;
      }

      body.data.forEach(data => {
        const post = new Post();
        const { id, content, media } = data;
        post.id = id;
        post.content = content;
        post.media = media;
        post.user = new User(data.user.id, data.user.name);

        this.visiblePosts.push(post);
      });
    })
  }
}

/**
 *
 */
class Thread { // eslint-disable-line

}

class SessionStore {
  static _instance = null;

  @observable accessToken = null;

  static instance() {
    if (SessionStore._instance) {
      return SessionStore._instance;
    }

    const fbAppId = localStorage.getItem('socialite.fbAppId');
    const accessToken = localStorage.getItem('socialite.accessToken');

    SessionStore._instance = new SessionStore({ fbAppId, accessToken });
    return SessionStore._instance;
  }

  constructor({ fbAppId, accessToken }) {
    this.fbAppId = fbAppId;
    this.fbsdk = FBSDK({
      appId: fbAppId,
      status: true,
      version: 'v3.0'
    });

    this.accessToken = accessToken;
  }

  @action login() {
    window.open(`https://www.facebook.com/v3.0/dialog/oauth?`
      + `client_id=${this.fbAppId}`
      + `&display=popup`
      + `&redirect_uri=${OAUTH_REDIRECT_BASE_URI}/auth/facebook`);

    console.log('Logging in...');
    window.onmessage = (e) => {
      this.accessToken = e.data;
      localStorage.setItem('socialite.accessToken', e.data);
    };
  }

  @computed get isLoggedIn() {
    return !!this.accessToken;
  }
}
/**
 *
 */
class AppStore {
  constructor() {
    this.storyline = new StoryLine();
  }

  @action createPost(data) {
    if (!SessionStore.instance().isLoggedIn) {
      throw new Error('Authorization required');
    }

    let uploadP;

    if (data.uploads.length) {
      const { uploads } = data;
      const form = new FormData();
      uploads.forEach(file => {
        form.append('media', file, file.name);
      });

      uploadP = fetch(`${API_BASEURL}/media/upload`, {
        method: 'POST',
        body: form,
        headers: {
          'Authorization': `Bearer ${SessionStore.instance().accessToken}`,
        }
        // headers: {
        //   'Content-Type': 'multipart/form-data; boundary=—-WebKitFormBoundaryBogus123456'
        // }
      }).then(res => res.json()).then((res) => {
        console.log(res);
        return res.filenames;
      })
    } else {
      uploadP = Promise.resolve(null);
    }

    return uploadP.then((filenames) => {
      if (filenames && filenames.length) {
        data.media = filenames;
      }

      fetch(`${API_BASEURL}/posts`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${SessionStore.instance().accessToken}`,
        },
        body: JSON.stringify(data)
      }).then(res => {
        if (res.ok) {
          return res.json()
        } else {
          throw new Error('Could not post');
        }
      }).then(body => {
        const { ok } = body;
        if (!ok) {
          console.warn('Could not load newly post');
          return;
        }

        const post = new Post();
        const { data: {id, content, user, media }} = body;
        post.id = id;
        post.content = content;
        post.media = media;
        post.user = new User(user.id, user.name);

        this.storyline.visiblePosts.unshift(post);
        return true;
      });
    });
  }
}

export const makeAppStore = () => new AppStore();
export const makeSessionStore = ({ fbAppId }) => {
  localStorage.setItem('socialite.fbAppId', fbAppId);
  return SessionStore.instance();
}
