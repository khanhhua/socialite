import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import './App.scss';
import SpeakBox from './speakbox';
import StoryLine from './storyline';

@inject("sessionStore")
@observer
class App extends Component {
  render() {
    const { sessionStore } = this.props;

    return (
      <div className="App">
        <header className="App-header">
          {`Socialite v${process.env.VERSION}`}
        </header>
        {!sessionStore.isLoggedIn &&
          <div className="container">
            <div className="row">
              <div className="col wide">
                <p>Login with Facebook</p>
                <div className="text-center">
                  <button className="btn btn-lg btn-primary" onClick={() => sessionStore.login()}>Login</button>
                </div>
              </div>
            </div>
          </div>
        }
        {sessionStore.isLoggedIn &&
        [
          <SpeakBox key="speakbox" />,
          <StoryLine key="storyline" />
        ]}
      </div>
    );
  }
}

export default App;
