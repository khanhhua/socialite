import React, { Component } from 'react';
import SimplePost from './SimplePost';
import AlbumPost from './AlbumPost';
import { observer, inject } from 'mobx-react';

import "./storyline.scss";

@inject("appStore")
@observer
export default class StoryLine extends Component {
  onLoadMorePosts = (e) => {
    const { storyline } = this.props.appStore;

    storyline.loadmore();
  }

  componentWillMount() {
    this.props.appStore.storyline.refresh();
  }

  render() {
    const visiblePosts = this.props.appStore.storyline.visiblePosts;

    return (
      <div className="storyline">

        {visiblePosts.map((post, index) => <AbstractPost key={`post-${index}`} post={post} />)}

        <div className="storyline__actions text-center">
          <div className="btn btn-outline-dark btn-sm" onClick={this.onLoadMorePosts}>Load older posts</div>
        </div>
      </div>
    );
  }
}

const AbstractPost = (props) => {
  const { post } = props;
  if (post.media && post.media.length) {
    return <AlbumPost {...props} />;
  } else {
    return <SimplePost {...props} />;
  }
}