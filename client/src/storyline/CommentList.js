import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import {
  sanitize,
  makeError,
  checkText,
  ERROR_EMPTY_VIOL,
  ERROR_EMPTY_MSG,
  ERROR_CONTENT_VIOL,
  ERROR_CONTENT_MSG,
  ERROR_LONG_TEXT_VIOL,
  ERROR_LONG_TEXT_MSG,
  RULE_CHARS_LIMIT
  } from '../validations';

@inject('appStore')
@observer
export default class CommentList extends Component {
  state = {
    comment: ''
  }

  componentWillMount() {
    const { post } = this.props;

    post.loadComments();
  }

  onTextareaChange = ({ target: { value } }) => {
    this.setState({
      comment: value,
      error: null
    });
  }

  onComment = () => {
    const { comment } = this.state;
    const santized = sanitize(comment);

    let error = validate(santized);
    if (error) {
      this.setState({
        error
      });
      return;
    }

    this.props.post.addComment({ content: santized }).then(ok => {
      if (ok) {
        this.setState({ comment: '' });
      }
    });
  }

  onPreviousComments = () => {
    this.props.post.loadComments();
  }

  render() {
    const { post } = this.props;
    const { comment, error } = this.state;

    return (
      <div className="comment-list">
        {!this.props.post.reachedBOF &&
        <div className="comment-list__actions text-center">
          <button data-test-id="button-older-comments"
                  className="btn btn-sm btn-link"
                  onClick={this.onPreviousComments}>Older comments
          </button>
        </div>
        }
        <div className="comment-list__comments">
          {post.comments.map((comment, index) => (
            <div className="comment" key={`comment-${comment.id}`}>
              <div className="comment__user" title={comment.user.name}>
                {comment.user.name}
              </div>
              <div className="comment__content">
                {comment.content}
              </div>
            </div>
          ))}
        </div>
        <div className="new-comment">
          <textarea data-test-id="comment"
                    className={`form-control ${!!error && 'is-invalid'}`} cols="30" rows="2"
                    onChange={this.onTextareaChange} value={comment}
                    placeholder="Speak up your mind!">
          </textarea>
          {error &&
          <div className="invalid-feedback">
            {error.message}
          </div>
          }
          <div className="new-comment__actions clearfix">
            <button data-test-id="button-post"
                    className="btn btn-sm btn-primary float-right"
                    disabled={!!error}
                    onClick={this.onComment}>POST
            </button>
          </div>
        </div>
      </div>
    )
  }
}

const validate = (comment) => {
  if (!comment) {
    return makeError(ERROR_EMPTY_VIOL, ERROR_EMPTY_MSG);
  }

  if (comment.length > RULE_CHARS_LIMIT) {
    return makeError(ERROR_LONG_TEXT_VIOL, ERROR_LONG_TEXT_MSG);
  }

  // Content issues
  if (!(/^[\w\s!@#$%^&*():;"'<=>+.,?/]*$/gm.test(comment))) {
    return makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG);
  }

  if (!checkText(comment)) {
    return makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG);
  }

  return false;
}