import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'mobx-react';
import './index.scss';

import { makeAppStore, makeSessionStore } from './store';
import App from './App';

// import registerServiceWorker from './registerServiceWorker';
// registerServiceWorker();

window.SOCIALITE = (root, { fbAppId }) => ReactDOM.render(
  <Provider appStore={makeAppStore()} sessionStore={makeSessionStore({ fbAppId })}>
    <App />
  </Provider>,
  document.getElementById(root));
